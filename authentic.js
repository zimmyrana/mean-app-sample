(function() {

    var _mongoClient = null;

    function getDbClient(cb) {
        if (!_mongoClient) {
            var db_url = "mongodb://admin:admin@ds149138.mlab.com:49138/mean-sample-db"
            if (db_url) {
                var MongoClient = require('mongodb').MongoClient;
                MongoClient.connect(db_url, function(err, db) {
                    if (err) {
                        console.error('Error retrieveing Mongo DB Client ' + err.toString());
                    } else {
                        _mongoClient = db;
                        _mongoClient.on('close', function() {
                            _mongoClient = null;
                        });
                    }
                    try {
                        cb(err, _mongoClient);
                    } catch (unhandledErr) {
                        console.error('Unhandled Error while returning connection from mongodb. Error: ', unhandledErr);
                    }
                });
            } else {
                console.error("Error: DB url not configured.");
            }
        } else {
            try {
                cb(null, _mongoClient);
            } catch (unhandledErr) {
                console.error('Unhandled Error while returning connection from mongodb. Error: ', unhandledErr);
            }
        }
    }

    module.exports = getDbClient;

        

})()