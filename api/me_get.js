(function() {

    function execute(req, res, next, helpers) {

        var manager = require('../data_access/managers').Managers;

        manager.getManagersList({"has_access":true}, {}, helpers, function(response) {
            res.send(response)
        })

    }

    exports.execute = execute
})()