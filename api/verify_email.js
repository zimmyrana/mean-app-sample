(function() {
    var Managers = require('../data_access/managers').Managers;


    function verifyEmail(rqst, res, next, helpers) {
        var ObjectID = require('mongodb').ObjectID;
        var id = rqst.params.id;
        var objid = new ObjectID(id);

        var uuid = rqst.body.uuid;
        var is_verified = rqst.body.is_verified;
        
        Managers.updateManagers({ _id: objid }, { $set: { uuid: "", is_verified: true, has_access: true } }, helpers, function(response) {

            //res.json(response);
            res.send("Msg : Verification process successfull");

        })

    }

    exports.verifyEmail = verifyEmail;
})()