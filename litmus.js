var express = require('express')
var app = express()
var bodyparser = require('body-parser');
var morgan = require('morgan');
var validate_user_post = require('./api/validate_user_post')
var helpers = require('./helpers').helpers;
var cors = require('cors') 
app.use(cors())
var port = process.env.PORT || 5000;
//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));


app.use(morgan('dev'));
app.use('/api',validate_user_post);
require('./router').establishRoutes(app, helpers);

app.listen(port)
console.log('port number is running '+port)	